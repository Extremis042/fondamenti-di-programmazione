#include <iostream>
using namespace std;

int main(){
	
	/*
		UTILIZZO DEL CICLO DO-WHILE
	*/
	int conta=0;
	do{
		
		printf("Buongiorno kaffeeeeee %d \n",conta);
		conta++;
	}while(conta < 15);
	conta=0;
	while(conta < 15){
		printf("KAFFFFFFFFFEEEEEEEEEE %d \n",conta);
		conta++;
	}
		
	return 0;
}
