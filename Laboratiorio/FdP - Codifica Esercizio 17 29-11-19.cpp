#include <iostream>
using namespace std;

int main(){
	string nome1,data1;
	string nome2,data2;
	int scelta;
	
	printf("Inserisci il nome della persona 1 \n");
	getline(cin,nome1);
	printf("Inserisci la data di nascita della persona 1 (usare sintassi dd/mm/aa) \n");
	getline(cin,data1);
	
	printf("Inserisci il nome della persona 2 \n");
	getline(cin,nome2);
	printf("Inserisci la data di nascita della persona 2 (usare sintassi dd/mm/aa) \n");
	getline(cin,data2);
	
	printf("Scrivere '0' se si vuole ordinare per data oppure scrivere '1' se si vuole ordinare per nome \n");
	cin >> scelta;
	if(scelta==0){
			// frammentazione della data
			string gg= data1.substr(0,1); // giorno
			string mm= data1.substr(3,4); // mese
			string aa= data1.substr(6,7); // anno
			
			string gg2= data2.substr(0,1); // giorno
			string mm2= data2.substr(3,4); // mese
			string aa2= data2.substr(6,7); // anno
			
			
			
			if(aa<aa2){
				cout << data1 << " " << data2;
			}else if(aa==aa2){
				
				if(mm<mm2){
					cout << data1 << " " << data2;
				}else if(mm==mm2){
					if(gg<gg2){
						cout << data1 << " " << data2 << "\n";
					}else{
						cout << data2 << " " << data1 << "\n";
					}	
				}else{
					cout << data2 << " " << data1 << "\n";
				}
			}else{
				cout << data2 << " " << data1 << "\n";
			}
	}else{
	
			if(nome1<nome2){
				cout << nome1 << " " << nome2 << "\n";
			}else{
				cout << nome2 << " " << nome1 << "\n";
			}
	}
	
	return 0;
}
