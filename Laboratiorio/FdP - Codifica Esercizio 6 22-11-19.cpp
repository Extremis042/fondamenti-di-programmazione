#include <iostream>
using namespace std;

#define euroDollaro 1.3676
const float euroLire=1936.27;
int main(){
	/*
		Esercizio valuta
		Effettuare la conversione delle valute in Euro e Dollaro
	*/
	float valore1,valore2;
	
	valore1=100/euroLire; // Effettuando la conversione da 100 lire in euro
	valore2=valore1*euroDollaro; //Effettuando la conversione da euro a dollaro
	cout << "Il calcolo di 100 lire in Euro e' di : \n" << valore1 <<"\n Il calcolo in dollari risulta invece: \n"<< valore2 << "\n\n\n";
	
	valore1=100000/euroLire; //Conversione 100000 lire in Euro
	valore2=valore1*euroDollaro;
	cout << "Il calcolo di 100000 lire in Euro e' di : \n" << valore1 <<"\n Il calcolo in dollari risulta invece: \n"<< valore2 << "\n\n\n";

	valore1=1000000/euroLire; //Conversione 100000 lire in Euro
	valore2=valore1*euroDollaro;	
	cout << "Il calcolo di 1000000 lire in Euro e' di : \n" << valore1 <<"\n Il calcolo in dollari risulta invece: \n"<< valore2 << "\n\n\n";

	system("pause");
	
	return 0;
}
