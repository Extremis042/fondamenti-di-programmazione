#include <iostream>
using namespace std;

int main(){
	/*
		CONSEGNA
		Controlli sui caratteri
	*/
	
	char carInput;
	int scelta=0;
	while(scelta!=1){
		cout << "Inserisci una cifra decimale: ";
		cin >> carInput;
		
		if( isdigit(carInput)){
			cout << "Hai inserito un numero \n";
		}else{
			cout << "Hai inserito una lettera, inserisci un numero \n";
		}
		cout << "Vuoi terminare? 1 SI - 0 NO \n";
		cin >> scelta;
	}
	
	return 0;
}
