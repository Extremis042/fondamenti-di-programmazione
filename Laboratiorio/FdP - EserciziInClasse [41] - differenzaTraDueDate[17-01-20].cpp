#include <iostream>
using namespace std;

void calcoloDifferenza(int totaleSec ,int totale[]);
int calcoloDifferenzaSec(int primaDataOra, int primaDataMin, int primaDataSec, int secondaDataOra, int secondaDataMin, int secondaDataSec, int totale[]);
bool calcoloErrori(int primaDataOra, int primaDataMin, int primaDataSec, int secondaDataOra, int secondaDataMin, int secondaDataSec);
int main(){
	/*
		CONSEGNA:
		Scerivere un programma che dati due orari espressi in ore minuti secondi calcolare la differenza ed esprimerla in secondi.
		
		Calcolare la stessa differenza ed esprimerla in ore, minuti e secondi.
		Usre due funzioni diverese per il calcolo indicandole con diff_sec() e diff_oms()
	*/
	
	int primaDataMin,primaDataSec, primaDataOra ,secondaDataMin,secondaDataSec ,  secondaDataOra,totaleSec;
	int totale[2];
	bool errore=0;
	cout << "-- CALCOLO DIFFERENZA TRA 2 DATE IN SECONDI  -- \n\n";
	
	while (errore!=1){
		cout << "Inserire la prima data secondo la dicitura ( ORA : MINUTI), senza ':' \n";
		cin >> primaDataOra >> primaDataMin >> primaDataSec;
	
		cout << "Inserire la prima data secondo la dicitura ( ORA : MINUTI), senza ':' \n";
		cin >> secondaDataOra >> secondaDataMin  >> secondaDataSec;
		errore=1;
		//errore=calcoloErrori(primaDataOra,secondaDataOra,primaDataMin,secondaDataMin,primaDataMin,secondaDataSec);
	}
	totaleSec=calcoloDifferenzaSec(primaDataOra,primaDataMin,primaDataSec,secondaDataOra,secondaDataMin,secondaDataSec,totale);
	calcoloDifferenza(totaleSec, totale);
	
	
	cout << "La differenza tra le due ore e' di : " << totale [0] << "(ore) : "<< totale[1] << " (min) :" << totale[2] << "(sec) \n";
	cout << "La differenza tra le due ore in secondi e' di : " << totaleSec << endl;
	
	return 0;
}

void  calcoloDifferenza(int totaleSec,int totale[]){


	totale[0]=totaleSec/3600;
	totale[1]=(totaleSec%3600)/60;
	totale[2]=(totaleSec%3600)%60;
}

int calcoloDifferenzaSec(int primaDataOra, int primaDataMin, int primaDataSec, int secondaDataOra, int secondaDataMin, int secondaDataSec, int totale[]){
	int totaleSec,ora,min,sec;
	primaDataOra=primaDataOra*3600;
	secondaDataOra=secondaDataOra*3600;
	
	primaDataMin=primaDataMin*60;
	secondaDataMin=secondaDataMin*60;
	
	ora=primaDataOra-secondaDataOra;
	min=primaDataMin-secondaDataMin;
	sec=primaDataSec-secondaDataSec;
	
	totaleSec=ora+min+sec;

	return totaleSec;
}

bool calcoloErrori(int primaDataOra, int primaDataMin, int primaDataSec, int secondaDataOra, int secondaDataMin, int secondaDataSec){
	bool errore=0;
	if(secondaDataOra> primaDataOra){
		errore=1;
		cout << "Non e' consentito inserire la seconda ora maggiore della prima \n";
	}if(primaDataOra>24 || secondaDataOra>24){
		cout << "Non e' consentito inserire ore maggiori di 24 \n";
	}else if(primaDataMin>60 || secondaDataMin>60){
		cout << "Non ' consentito inserire min maggiori di 60 \n";
	}else if(primaDataSec>60 || secondaDataSec>60){
		cout << "Non e' consentito inserire secondi maggiori di 60 \n";
	}
	return errore;
}

