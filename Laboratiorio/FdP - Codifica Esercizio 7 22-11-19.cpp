#include <iostream>
using namespace std;

int main(){
	/*
		Dimensione e calcolo delle dimensioni delle variabili
	*/
	printf("La dimensione della variabile di tipo Char e' di %d \n",sizeof(char));
	printf("La dimensione della variabile di tipo Short e' di %d \n",sizeof(short));
	printf("La dimensione della variabile di tipo Int e' di %d \n",sizeof(int));
	printf("La dimensione della variabile di tipo Long e' di %d \n",sizeof(long));
	printf("La dimensione della variabile di tipo Float e' di %d \n",sizeof(float));
	printf("La dimensione della variabile di tipo Double e' di %d \n",sizeof(double));
	printf("La dimensione della variabile di tipo Long Long e' di %d \n",sizeof(long long));
	printf("La dimensione della variabile di tipo Long Double e' di %d \n",sizeof(long double));
	printf("La dimensione della variabile di tipo Puntatore a caratteri char* e' di %d \n",sizeof(char*));




	return 0;
}
