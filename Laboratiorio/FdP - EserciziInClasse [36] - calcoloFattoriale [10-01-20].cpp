#include <iostream>
using namespace std;

double fattoriale(int x){
	double i;
	double f=1;
	
	for (i=1; i<=x; i++){
		f=f*i;
	}
	return f;
}

int main(){
	
		/*
		CONSEGNA:
			- Esercizio che esegue lo scambio del valore di due variabili
	*/
	
	int base;
	double ris;
	
	cout << "-- Calcolo del fattoriale di un numero -- \n";
	cout << "Inserisci la base: \n";
	cin >> base;
	
	ris=fattoriale(base);
	
	cout << "Stampa del valore: " << base << " = " << ris << endl;
	
	return 0;
}
