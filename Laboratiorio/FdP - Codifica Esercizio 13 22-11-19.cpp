#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main(){
	/*
		Random
		Uso della funzione random per simulare il lancio di un dado;
		
		rand() - Funzione che genera il numero casuale seguendo la sintassi:
				SYNTAX:
					var=(rand()%6)+1
	*/
	int n;
	srand(time(NULL));
	n=(rand()%6)+1;
	printf("Il risultato del dado e' di %d \n ",n);
	
	
	return 0;
}
