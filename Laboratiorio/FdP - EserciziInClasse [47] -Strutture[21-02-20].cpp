#include <iostream>
#define MAX 100

using namespace std;

struct Libro{
	string titolo,autore,editore;
	int numpagine;
	double prezzo;
};

void stampaVet(Libro contenitoreLibri[], int numeroLibri);
int dimensionaVet();
void riempiVet(Libro contenitoreLibri[], int numeroLibri);
void ordinaVet(Libro contenitoreLibri[], int numeroLibri);

Libro insertLibro();
/*
	CONSEGNA:
	Scrivere un programma per ralizzare una tabella di libri; utilizzare le funzioni per realizzare il programma:
		Funzione per inserire i dati di un libro da input
		Funzione per visualizzare i dati di un libro
		Funzione per inserire un libro in array
		Funzione per stampare l'elenco dei libri
		Funzione per stabilire quanti libri si vuole inserire
*/



int main(){
	Libro contenitoreLibri[MAX];
	
	//Richiesta all'utente di quantilibri vuole inserire
	int numeroLibri;
	
	//Uso di 1 funzione per calcolare il numero di elementi del vettore utilizzata
	numeroLibri=dimensionaVet();
	riempiVet(contenitoreLibri,numeroLibri);
	stampaVet(contenitoreLibri,numeroLibri);
	system("PAUSE");

}

void stampaVet(Libro contenitoreLibri[], int numeroLibri){
	cout << "\n\n-- Stampa dei dati dei libri\n";
	for(int i=0; i<numeroLibri; i++){
		cout << "\t I dati del libro "<< i+1 << " sono : \n";
		cout << "\t\t Titolo: " << contenitoreLibri[i].titolo << endl;
		cout << "\t\t Autore: " << contenitoreLibri[i].autore << endl;
		cout << "\t\t Editore: " << contenitoreLibri[i].editore << endl;
		cout << "\t\t Numero Pagine: " << contenitoreLibri[i].numpagine << endl;
		cout << "\t\t Prezzo: " << contenitoreLibri[i].prezzo << endl << endl;
		
	}
}

int dimensionaVet(){
	int n;
	do{
		cout << "Inserire il valore massimo di libri utilizzati (Max : "<< MAX<< ") \n";
		cin >> n;
		
		if(n>MAX || n<=0){
			cout << "Errore, il numero di elementi inseriti non rispetta le condizioni.";
		}
		
	}while(n>MAX || n<=0);
	
	return n;
}

Libro insertLibro(){
	Libro l;
	
	fflush(stdin);
	cout << "\n\n-- Inserimento dei dati di un libro.\n";
	cout << "\t Inserisci il titolo del libro: ";
	getline(cin,l.titolo);
	
	cout << "\t Inserisci il l'autore del libro: ";
	getline(cin, l.autore);
	
	cout << "\t Inserisci l'editore del libro: ";
	getline(cin, l.editore);
	
	cout << "\t Inserisci il numero di pagine del libro: ";
	cin >> l.numpagine;
	
	cout << "\t Inserisci il prezzo del libro: ";
	cin >> l.prezzo;
	
	return l;
}

void riempiVet(Libro contenitoreLibri[], int numeroLibri){
	for(int i=0; i<numeroLibri; i++){
		Libro l = insertLibro();
		contenitoreLibri[i]=l;
	}
}

void ordinaVet(Libro contenitoreLibri[], int numeroLibri){
	string temp;
	for(int i=0; i<numeroLibri-1;i++){
		for(int c=i+1; c<numeroLibri; c++){
			if(contenitoreLibri[i].editore > contenitoreLibri[c].editore){
				temp=contenitoreLibri[i].editore;
				contenitoreLibri[i].editore=contenitoreLibri[c].editore;
				contenitoreLibri[c].editore=contenitoreLibri[i].editore;
			}
		}
	}
	
}

