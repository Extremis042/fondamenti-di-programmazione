#include <iostream>
#include <cmath>
using namespace std;

int main(){
	/*
		Una leggenda orientale narra di un matematico che, in cambio di alcuni servigi resi al re, chiese la seguente ricompensa
		un chicco di riso per la prima casella di una scacchiera, due chicchi di riso per la seconda casella di una scacchiera, 
		quattro chicchi di riso per la terza casella, e cos� via per titte e 64 caselle della scacchiera.
		
		Progettare un algorimo che, a partire dal numero N di caselle che si intendono riempire, calcoli il numero complessivo di
		chicchi di riso che spettano come ricompensa
	*/
	int n,i=1, ricompensa=0;
	cout << "Inserire la quantita' di caselle in questione: \n";
	cin >> n;
	
	while (i<=n){
		ricompensa=ricompensa+pow(2,i-1);
		i++;
	}
	cout << "La ricompensa totale e' di: " << ricompensa << endl;

	return 0;
}
