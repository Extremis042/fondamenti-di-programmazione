#include <iostream>
#define maxV 10

using namespace std;


void caricaMat(int mat[maxV][maxV]);
void stampaMat(int mat[maxV][maxV]);

int main(){
	int mat [maxV] [maxV];
	
	caricaMat(mat);
	stampaMat(mat);
}


void caricaMat(int mat[maxV][maxV]){
	int val=1;
	
	for(int i=0; i<maxV; i++ ){
		for(int c=0; c< maxV; c++){
			mat[c][i]=(c+1)*val;	
		}
		val++;
	}
	
}

void stampaMat(int mat[maxV][maxV]){
	for(int i=0; i<maxV; i++ ){
		for(int c=0; c< maxV; c++){
			cout << mat[c] [i] << "\t" ;
		}
		cout << endl << endl;
		
	}
}
