#include <iostream>
using namespace std;

int main(){
	/*
		ORDINE ALFABETICO
		Dati 2 nomi, li stampa in ordine alfabetico
	*/
	string nome1,nome2;
	printf("Inserisci il primo ed il secondo nome separati da un invio.\n");
	getline(cin,nome1);
	getline(cin,nome2);
	
	
	if(nome1<nome2){
		cout << nome1 << " "<< nome2 <<"\n";
	}else{
		cout << nome2 << " " << nome1 << "\n";
	}
	return 0;
}
