#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;
#define dimensioneMax 30

void caricaVettore(int vet[], int n);
void stampaVettore(int vet[], int n);
void copiaVettore(int vet[], int n, int vetC[]);

int main(){
	/*
		CONSEGNA:
		Scrivere un Programma che usa le funzioni per
			1) caricare un array di numeri random da 1 a 100
			2) stampare in modo sequenziale un array
			3) copiare l�array in un altro array
			4) stampare la copia dellarray
	*/
	
	int vet[dimensioneMax] ,  copiaVet[dimensioneMax]; 
	int n;
	
	cout << "Inserire il numero di elementi in cui si vuole generare il numero casuale\n";
	cin >> n;
	
	caricaVettore(vet,n);
	stampaVettore(vet,n);
	
	cout << "Effettuo copia dell'array .. \n";
	copiaVettore(vet,n,copiaVet);
	
	stampaVettore(copiaVet,n);
}

void caricaVettore(int vet[], int n){
	int numeroCasuale;
	
	for (int i=0; i<n; i++){
		numeroCasuale=(rand()%100)+1;	
		vet[i]=numeroCasuale;
		srand(time(NULL));
	}
	
}


void stampaVettore(int vet[], int n){
	for (int i=0; i<n; i++){
		cout << "il valore della casella: " << i << " e' di : " << vet[i] << endl;
	}
	
}

void copiaVettore(int vet[], int n, int vetC[]){
	for(int i=0; i<n; i++){
		vetC[i] = vet[i];
	}

}
