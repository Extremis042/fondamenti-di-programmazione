#include <iostream>
using namespace std;

int main(){
	/*
		CONSEGNA:
		
		Esercizio che controlla quanti numeri sono pari o dispari, con richiesta se si vuole continuare(?)
		
		
	*/
	int i=1,num,supVar;
	int numPari=0,numDispari=0;
	while(i>0){
		cout << "Inserisci un numero da processare \n";
		cin >> num;
		supVar=num%2;
		
		if(supVar==0){
			numPari++;
		}else if (supVar==1){
			numDispari++;
		}
		cout << "Continuare l'operazione? Inserire 1 per SI, inserire 0 per NO \n";
		cin >> i;
	}
	
	cout << "Numeri dispari: " << numDispari << "\n Numeri pari: "<< numPari << endl;
	
	return 0;
}
