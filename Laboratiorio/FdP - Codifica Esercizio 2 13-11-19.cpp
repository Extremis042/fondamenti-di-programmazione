#include <iostream>
using namespace std;

int main() {
	//Scambia 2 valori
	int num1,num2,supp=0;
	cout << "inserisci il primo numero \n";
	cin >> num1;
	cout << "Inserisci il secondo numero \n";
	cin >> num2;
	
	cout << "Hai inserito " << num1 << " come primo numero e " << num2 << " come secondo numero \n";
	
	supp=num1;
	num1=num2;
	num2=supp;
	
	cout << "I numeri scambiati sono: "<< num1 << " "<< num2;
	return 0;
}
