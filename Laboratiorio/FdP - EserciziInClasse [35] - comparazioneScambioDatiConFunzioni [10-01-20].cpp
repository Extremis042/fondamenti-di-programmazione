#include <iostream>
using namespace std;

void scambia1(int primoNumero, int secondoNumero);
void scambia2(int &primoNumero, int &secondoNumero);


int main(){
	/*
		CONSEGNA:
			- Esercizio che esegue lo scambio del valore di due variabili
	*/
	
	int primoNumero,secondoNumero , primo,ultimo;
	
	cout << "\n Inserisci il primo numero intero: \n";
	cin >> primoNumero;
	
	cout << "\n Inserisci il secondo numero rintero: \n";
	cin >> secondoNumero;
	
	cout << "Richiamo la funzione scambia(), passando parametri per valore \n";
	scambia1(primoNumero,secondoNumero);
	
	cout << "Stampa dei valori scambiati: " << "primo numero= "<< primoNumero << " secondo numero= " << secondoNumero << endl;
	
	cout << "Richiamo la funzione scambia(), passando parametri per valore \n";
	scambia2(primoNumero,secondoNumero);
	
	cout << "Stampa dei valori scambiati: " << "primo numero= "<< primoNumero << " secondo numero= " << secondoNumero << endl;
	return 0;
}

void scambia1(int primoNumero, int secondoNumero){
	int temp;
	temp=primoNumero;
	primoNumero=secondoNumero;
	secondoNumero=temp;
	
}

void scambia2(int &primoNumero, int &secondoNumero){
	int temp;
	temp=primoNumero;
	primoNumero=secondoNumero;
	secondoNumero=temp;
}
