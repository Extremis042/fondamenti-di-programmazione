#include <iostream>
#include <cmath>
using namespace std;

int main(){
	
	/*
		Funzioni matematiche
		
		abs() - Calcolo del valore assoluto, restituzione del valore senza segno 
		sqrt() - Calcolo della radice quadrata
		ceil() - Calcolo del numero intero non minore di a (arrotonda per eccesso al numero sucessivo)
		floor() - Calcola il numero intero NON maggiore di a (arrotonda al numero precedente )
		log() - Calcola il logaritmo naturale di a
		logN() - Calcolo del logaritmo alla N di a
		pow(var,esponente) - Calcolo della potenza 
	*/
	float a=100.15;
	
	cout << abs(-a) << endl;
	cout << sqrt(a) << endl;
	cout << ceil(a) << endl;
	cout << floor(a) << endl;
	cout << log(a) << endl;
	cout << log10(a) << endl;
	cout << pow(a,2) << endl;
	return 0;
}
