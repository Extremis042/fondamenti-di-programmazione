#include <iostream>
using namespace std;

int main(){
	//Ripasso SCANF
	int iVar1,iVar2;
	unsigned int iVarUnsgnd;
	float fVar;
	char cVar;
	
	printf("Inserisci un numero intero negativo, un numero intero positivo e un numero con la virgola.\n");
	scanf("%d %u %f",&iVar1, &iVarUnsgnd, &fVar);
	
	//NecessitÓ di effettuare un flush per svuotare il buffer
	
	fflush(stdin);
	printf("Inserisci un carattere \n");
	scanf("%c",&cVar);
	

	printf("Le variabili sono: \n %d - Numero intero neg. \n %u - Numero positivo \n %f - Numero con la virgola \n %c - Carattere",iVar1,iVarUnsgnd,fVar,cVar);
	return 0;
}
