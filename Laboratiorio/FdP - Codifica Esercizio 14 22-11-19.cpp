#include <iostream>
using namespace std;

int main(){
	/*
		Gestione delle stringhe
		string.length() - calcolo della lunghezza della stringa
		string.at(N);	- Restituisce il carattere posizionato alla posizione n
		string.substr(n,n1) - crea una sottostring compresa dalle 2 posizioni
		string=string+"testo" - Aggiunta veloce del testo
	*/
	string s ="test della stringa";
	string s2 = string("test della stringa");
	string st = "test";
	cout << st << endl;
	int n = st.length();
	cout << "numero di caratteri di st = "<< n << endl;
	char p=st.at(1);
	return 0;
}
