#include <iostream>
using namespace std;

int main(){
	/*
		CONSEGNA
		Scrivere un programma che legga N valori numerici in input finche' la soma e' minore di 100
		stampare la somma e quanti numeri sono stati letti.
			
	*/
	
	int numero,somma=0,i=0;
	do{
		cout << "Inserire un numero: \n";	
		cin >> numero;
		somma=somma+numero;
		cout << endl;
		i++;
	}while(somma<100);
	
	cout << "La somma totale e': " << somma << endl;
	cout << "I numeri letti sono : " << i << endl;
	return 0;
}
