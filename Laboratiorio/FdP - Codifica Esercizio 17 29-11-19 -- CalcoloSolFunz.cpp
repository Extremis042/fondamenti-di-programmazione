#include <iostream>
using namespace std;
#include <math.h>
int main(){
	/*
		CALCOLO EQUAZIONE DI SECONDO GRADO
	*/
	float delta,ris,sqr,a,b,c;
	
	printf("Inserire i numeri (a,b,c) seguendo lo schema ax^2+bx+c \n");
	scanf("%f %f %f",&a,&b,&c);
	
	// Calcolo Delta

	delta=pow(b,2)-(4*a*c);
	

	if(delta<0){
		printf("Non ci sono soluzioni reali. \n");
	}else if(delta==0){
		ris=-b/2*a;
		printf("%f",&ris);
	}else{
		// 2 sol
		ris=(-b+sqrt(delta) )/2*a; //Sol1
		printf("La prima soluzione e': %f \n",ris);
		ris=(-b-sqrt(delta) )/2*a; //Sol1
		printf("La seconda soluzione e': %f \n",ris);
		
	}
	
	
	return 0;
}
