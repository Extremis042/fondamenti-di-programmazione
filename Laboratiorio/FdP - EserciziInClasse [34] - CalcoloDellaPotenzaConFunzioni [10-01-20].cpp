#include <iostream>
using namespace std;

int potenza(int b, int e);

int main(){
	/*
		CONSEGNA (Potenza):
			- calcolo della potenza utilizzando prototipi e funzioni
	*/
	int base,esponente,ris;
	
	cout << "Insertisci la base: ";
	cin >> base;
	
	cout << "Inserisci l'esponente: ";
	cin >> esponente;
	
	ris=potenza(base,esponente);
	
	cout << "Il risultato di " << base << " elevato a " << potenza << " e' di : "<< ris<<"\n";
	return 0;
}

int potenza(int b, int e){
	int ris=1;
	
	for(int i=1; i<=e; i++){
		ris=ris*b;
	}
	
	return ris;
}
