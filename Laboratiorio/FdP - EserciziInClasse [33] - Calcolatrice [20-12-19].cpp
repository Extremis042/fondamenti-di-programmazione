#include <iostream>
#include <cstdlib>
using namespace std;

int main(){
	/*
		CONSEGNA:
		Controllo dell'Input dell'utente con richiesta di conferma dei dati insieriti e poi esegue un�'operazione a scelta tra addizione, sottrazione, moltiplicazione o divisione.
		Questo programma chiede all'utente due numeri e il simbolo dell'operazione da eseguire:
			i simboli sono "+" o "-" o "*" o "/"
			
		Se i simboli immessi non sono quelli ammessi il programma permette di correggere l'input e permette di far terminare il programma premendo il simbolo "#"
	*/
	char sceltaUser,confermaUser;
	float primoNumero=-900.0,secondoNumero=-900.0,ris;
	do{
		cout << "--- CALCOLATRICE --- \n";
		
		do{
		
		cout << "Inserire i due numeri su cui effettuare le operazioni seguendo la sintassi: \n";
		cout << "num1 num2 \n";
		
		//controllo inserimento primo numero
		do{
			
			scanf("%f",&primoNumero);
			if(primoNumero==-900.0){
				cout << "Hai inserito un valore non consentito, inserisci un valore accettabile (intero). \n";
			}
			fflush(stdin);
		}while(primoNumero==-900);
		
		//controllo inserimento primo numero
		cout << "Inserisci il secondo numero \n";
		do{
			scanf("%f",&secondoNumero);
			if(secondoNumero==-900.0){
				cout << "Hai inserito un valore non consentito, inserisci un valore accettabile (intero) \n";
			}
			fflush(stdin);
		}while(secondoNumero==-900);
		
		cout << "Hai inserito: \n " << primoNumero << " come primo numero \n " << secondoNumero << " come secondo. \n";
		cout << "\t Scrivere Y per continuare, N per re-inserire i numeri \n";
		cin >> confermaUser;
		confermaUser=toupper(confermaUser); 
		}while(confermaUser!='Y' );
		
		
		cout << "\t inserire il tipo di operazione tra: \n";
		cout << "\t + -> addizione. \n";
		cout << "\t - -> sottrazione. \n";
		cout << "\t * -> moltiplicazione. \n";
		cout << "\t / -> divisione. \n";
		cout << "\t # -> esci \n";
		
		cin >> sceltaUser;
		if(sceltaUser=='#'){
			cout << "\t Uscita \n";
		}else{
		
		switch(sceltaUser){
			case '+':
				//Addizione
				ris=primoNumero+secondoNumero;
				cout << "\t\t Il risultato della somma e': " << ris << endl;
			break;
			
			case '-':
				ris=primoNumero-secondoNumero;
				cout << "\t\t Il risultato della sottrazione e': " << ris << endl;
			break;
			
			case '*':
				ris=primoNumero*secondoNumero;
				cout << "\t\t Il risultato della moltiplicazione e': " << ris << endl;
			break;
			
			case '/':
				if(secondoNumero!=0){
					ris=primoNumero/secondoNumero;
					cout << "\t\t Il risultato della divisione e': " << ris << endl;
				}else{
					cout << "\t \t Divisione per 0 non consentita. \n";
				}
			break;
			
			default:
				cout << "\t \t Inserisci un carattere tra quelli presenti in elenco. \n";
			break;
		}
	}
	}while(sceltaUser!='#');
	
	return 0;
}
