#include <iostream>
using namespace std;

int main(){
	/**
		Programma che dice se ho inserito una vocale o una consonante
	*/
	char lettera;
	
	cout << "Inserisci una lettera \n";
	scanf("%c",&lettera);
	/*
	if(lettera == 'A' || lettera=='a'){
		printf("Hai inserito una vocale: %c \n",lettera);
	}else if(lettera == 'E' || lettera=='e'){
		printf("Hai inserito una vocale: %c \n",lettera);
	}else if(lettera == 'I' || lettera=='i'){
		printf("Hai inserito una vocale: %c \n",lettera);
	}else if(lettera == 'O' || lettera=='o'){
		printf("Hai inserito una vocale: %c \n",lettera);
	}else if(lettera == 'U' || lettera=='u'){
		printf("Hai inserito una vocale: %c \n",lettera);
	} else{
		printf("Hai inserito una consonate. \n");
	}  
	*/
	
	/*oppure si utilizza lo switch*/
	
	switch(lettera){
		case 'a': case 'A':
			printf("Hai inserito la lettera %c",lettera);
		break;
		case 'e': case 'E':
			printf("Hai inserito la lettera %c",lettera);
		break;
		case 'i': case 'I':
			printf("Hai inserito la lettera %c",lettera);
		break;
		case 'o': case 'O':
			printf("Hai inserito la lettera %c",lettera);
		break;
		case 'u': case 'U':
			printf("Hai inserito la lettera %c",lettera);
		break;
		
		default:
			
		break;
	}
	
	return 0;
}
