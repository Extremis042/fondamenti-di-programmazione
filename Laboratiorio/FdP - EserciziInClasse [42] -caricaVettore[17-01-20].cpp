#include <iostream>
using namespace std;

void caricaVettore(int vet[], int n);
void stampaVettore(int vet[], int n);

int main(){
	/*
		CONSEGNA:
		Scrivere un Programma che usa le funzioni per
			1) Leggere da input la dimensione dell�array che si vuole realizzare
			2) Caricare l�array con valori presi in input dalla tastiera
			3) Stampare in modo sequenziale l� array 
	*/
	int n;
	
	cout << "Inserire la dimensione dell'array che si vuole realizzare \n";
	cin >> n;
	
	int vet [n];
	
	caricaVettore(vet,n);
	
	stampaVettore(vet,n);
	
}

void caricaVettore(int vet[], int n){
	for(int i=0; i<n; i++){
		cout << "Inserisci il valore\n";
		cin >> vet[i];
	}
	
}

void stampaVettore(int vet[], int n){
	for (int i=0; i<n; i++){	
		cout << "Il valore nella casella " << i << " e' di : " << vet[i]<< endl; 
	}
}
