#include <iostream>
#include <cstdlib>
#include <ctime>
#define maxTentativi 7

using namespace std;

int main(){
	/*
		CONSEGNA
		Indovinare un numero scelto dal PC con un numero di tenativi fissati
		
	*/
	
	int numero, i=0, tentativo;
	
	cout << "GIOCO: Indovina numero \n";
	srand(time(NULL));
	numero=(rand()%100)+1;
		
	while(i<maxTentativi && tentativo!= numero){
		cout << "Inserisci il numero; \n";
		cin >> tentativo;
		if(tentativo > numero){
			cout << "Il numero inserito e' maggiore rispetto al numero da indovinare. \n";
		}else if (tentativo < numero){
			cout << "Il numero inserito e' minore rispetto al numero da indovinare. \n";
		}
		
		i++;
	}	
		
	if(i<maxTentativi){
		cout << "Hai indovinato il numero, EPICO \n";
	}else{
		cout << "Ti puzzano i piedi \n";
	}
	return 0;
}
