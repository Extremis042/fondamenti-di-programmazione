#include <iostream>
using namespace std;

bool calcoloAnnoB(int anno){
	bool bisestile=0;
	
	if( (anno%4==0 && anno%100!=0 )){
		//annoBisestioe
		bisestile=1;
	}
	
	return bisestile;
}

int main(){
	
	int anno;
	bool calcoloAnno;
	
	cout << "Inserisci l'anno: 	\n";
	cin >> anno;
	
	calcoloAnno=calcoloAnnoB(anno);
	if(calcoloAnno==1){
		cout << "L'anno e' bisestile. \n";
	}else{
		cout << "L'anno non e' bisestile \n";
	}
	
	return 0;
}
