#include <iostream>
using namespace std;

void divisione(float x, float y, float &r, bool errore);
void leggi(float &a, float &b);

int main(){
	/*
		CONSEGNA:
			- Esecuzione di una divisione di due numeri
	*/
	
	float a,b,c;
	bool errore;
	
	cout << "-- Divisione tra due nuemri presi in input -- \n\n";
	leggi(a,b);
	divisone(a,b,c,errore);
	if(!errore){
		cout << a << " : " << b << " = " << c << endl;
	}else{
		cout << "Errore: impossibile eseguire la divisione per 0.";
	}
	
	return 0;
}

void leggi( float &a, float &b){
	cout << "Inserire il primo numero \n";
	cin >> a;
	
	cout << "Inserire il secondo numero";
	cin >> b;
}

void divisione( float x, float y, float &r, bool &errore){
	
	errore=false;
	if(y!=0){
		r=x/y;
	}else{
		errore=true;
	}
}
