#include <iostream>
#include <cstdlib>
#include <ctime>

#define maxV 5

using namespace std;

void riempiVettore(int vet[]);
void ordinaVettore(int vet[], int vetCopia[]);
void copiaVettore(int vet[], int vetCopia[]);
void ordinaVettoreBolle(int vet[], int vetCopia[]);
int contaValori(int valoreMinimo,int valoreMassimo, int vet[]);
int contaRipetuti(int vet[]);
void stampaVettore(int vet[]){
	
	for(int i=0;i<maxV;i++){	
		cout << vet[i] << endl;
	}
}
int main(){
	/*
	CONSEGNA:
		1. Scrivere una funzione1 che riempie un array di 100 elementi con numeri interi casuali
		2. Scrivere una funzione2 che ordina gli elementi dell�array secondo l�ordinamento Insertionsort e li conservi in un secondo array (il primo array deve rimanere non ordinato) .
		3. Scrivere una funzione3 che ordina gli elementi dell�array secondo l�ordinamento a bolle e li
		conservi in un altro array .
		
		4. Scrivere una funzione4 che conti quanti elementi sono compresi tra un valore minimo e un
		valore massimo presi in input
		
		5. Scrivere un funzione5 che calcoli se ci sono elementi ripetut
	*/
	
	int vet[maxV],vetCopia[maxV],vetCopia2[maxV];
	int vMin,vMax;
	
	riempiVettore(vet);
	stampaVettore(vet);
	system("PAUSE");
	
	ordinaVettore(vet,vetCopia);
	stampaVettore(vetCopia);
	
	system("PAUSE");
	ordinaVettoreBolle(vet,vetCopia2);
	stampaVettore(vetCopia2);
	
	cout << "Inserire il numero minimo di valori da contare";
	cin >> vMin;
	cout << "Inserire il valore massimo";
	cin >> vMax;
	
	cout << "Il numero di valori compresi tra " << vMin << " e "<< vMax << "e' di: "<< contaValori(vMin,vMax,vet); 
	
	cout << "\n Il numero di valori ripetuti sono di "<<contaRipetuti(vet) << endl;
	
	return 0;
}

void riempiVettore(int vet[]){
	srand ( time(0) );
	for(int i=0;i<maxV;i++){
		vet[i]=(rand()%100);	
	}
}

void ordinaVettore(int vet[],int vetCopia[]){
	int temp=0;
	
	copiaVettore(vet,vetCopia);
	for(int i=0; i<maxV-1; i++){
		for(int j=i+1; j<maxV; j++){
			if(vet[i]>vet[j]){
				vetCopia[i]=vet[j];
				vetCopia[j]=vet[i];
			}
		}
	}
}
void copiaVettore(int vet[], int vetCopia[]){
	for(int i=0; i<maxV; i++){
		vetCopia[i]=vet[i];
	}
}

void ordinaVettoreBolle(int vet[], int vetCopia[]){
	bool scambioEffettuato=true;
	int ultimo=maxV-1;
	int temp;
	
	copiaVettore(vet,vetCopia);
	
	while(scambioEffettuato==true){
		scambioEffettuato=false;
		
		for(int i=0;i<ultimo;i++){
			if(vetCopia[i]>vetCopia[i+1]){
				temp=vetCopia[i];
				vetCopia[i]=vetCopia[i+1];
				vetCopia[i+1]=temp;
				scambioEffettuato=true;
			}
		}
		
	}
}

int contaValori(int valoreMinimo,int valoreMassimo, int vet[]){
	int contaV=0;
	for(int i=0; i< maxV; i++){
		if(vet[i]>valoreMinimo && vet[i]<valoreMassimo){
			contaV++;
		}
	}
	
	return contaV;
	
}

int contaRipetuti(int vet[]){
	int contaD=0;
	
	for(int i=0; i<maxV-1; i++){
		for(int c=i+1; c<maxV; c++){
			if(vet[i]==vet[c]){
				contaD++;
			}
		}
	}
	return contaD;
}

