#include <iostream>
#include <conio.h>
using namespace std;

int main(){
	/*
		Input di caratteri
		funzioni get necessitano 
	*/
	char c1,c2,c3;
	printf("Inserisci un carattere con getch() (nasconde il carattere) \n");
	c1=getch();
	printf("Il carattere digitato e' %c \n",c1);
	
	cout << "Inserisci un carattere con getche (lo visualizza senza aspettare l'invio) \n";
	c2=getche();
	printf("Il carattere digitato e' : %c \n",c2);
	
	cout << "Inserisci un carattere con getchar() (viene visualizzato e richiede enter) \n";
	c3=getchar(); 
	printf("Il carattere digitato e' : %c \n",c3);
	system("pause");
	return 0;
}
