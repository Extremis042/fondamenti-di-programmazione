#include <iostream>
#define max 50
#include <cstdlib>
#include <ctime>

using namespace std;

void caricaVettore(int vet[], int n);
int calcolaMinimoVet(int vet[], int n);
int calcolaMassimoVet(int vet[], int n);
float calcoloMediaVet(int vet[], int n);
void stampaArray(int vet[], int n);


int main(){
	/*
		CONSEGNA:
		Scrivere un programma con l�uso di funzioni che:
			1. Riempie un array di numeri casuali da 1 a 100
			2. Calcola il minimo di tutti gli elementi
			3. Calcola il massimo di tutti gli elementi
			4. Calcola la media di tutti gli elementi
			5. Stampa l�array
		
	*/
	int vet[max],n, min,maxV;
	float media;
	
	cout << "Inserire il numero massimo di elementi da utilizzare \n";
	cin >> n;
	
	caricaVettore(vet,n);
	
	min=calcolaMinimoVet(vet,n);
	cout << "Il minimo e' di " <<  min << endl;
	
	maxV = calcolaMassimoVet(vet,n);
	cout << "Il massimo e' di " << maxV << endl;
	
	media = calcoloMediaVet(vet,n);
	cout << "La media e' di : "<< media << endl;
	cout << "\n\nStampa vettore :\n ";
	stampaArray(vet,n);
	 
}


void caricaVettore(int vet[], int n){
	int numeroCasuale;
	srand(time(0));
	for(int i=0; i<n; i++){
		numeroCasuale=(rand()%100)+1;	
		vet[i]=numeroCasuale;
	}
}

int calcolaMinimoVet(int vet[], int n){
	int min = vet[0];
	
	for (int i=1; i<n; i++){
		if(vet[i]< min){
			min=vet[i];
		}
	}
	return min;
}

int calcolaMassimoVet(int vet[], int n){
	int maxV = 0;
	
	for(int i=0; i<n; i++){
		if(vet[i] > maxV){
			maxV= vet[i];
			
		}
	}
	return maxV;
}

float calcoloMediaVet(int vet[], int n){
	float media=0;
	int i;
	
	for(i=0; i<n; i++){
		media = media + vet[i];
	}
	
	if(i!=0){
		media= media /i;
	}
	return media;
}

void stampaArray(int vet[], int n){
	int i;
	
	for(i=0; i<n; i++){
		cout << i << " -  " <<vet[i] << endl;
	}
}


