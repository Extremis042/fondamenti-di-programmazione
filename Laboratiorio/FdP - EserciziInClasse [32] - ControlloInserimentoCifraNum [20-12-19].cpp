#include <iostream>
using namespace std;

int main(){
	/*
		Controllo se � stata inserita una cifra numerica con uscita dal ciclo con un carattere
	*/
	
	int scelta=0;
	char carattere;
	
	while(scelta!='t'){
		cout << "Inserisci una cifra numerica oppure 't' per terminare";
		carattere=getchar();
		//isdigit ritorna V o F se il carattere � una cifra compresa tra 0 e 9
		if(isdigit(carattere) || carattere=='t'){
			if(carattere=='t'){
				cout << "Uscita dal ciclo ... \n";
			}else{
				cout << "Hai inserito una cifra numerica: " << carattere << endl;
			}
		}else{
			cout << "Errore. Ti puzzano gli osvaldi \n";
		}
		
		fflush(stdin);
	}
	
	
	return 0;
}
